resource "kubernetes_deployment_v1" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      test = "frontend"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          test = "frontend"
        }
      }

      spec {
        container {
          image = "493270667162.dkr.ecr.ap-southeast-1.amazonaws.com/aug-ffm-frontend:latest"
          name  = "frontend"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "frontend_svc" {
  metadata {
    name = "frontend"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.frontend.metadata.0.labels.test
    }
    
    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}
