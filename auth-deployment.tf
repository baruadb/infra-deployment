resource "kubernetes_deployment_v1" "auth" {
  metadata {
    name = "auth"
    labels = {
      test = "auth"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "auth"
      }
    }

    template {
      metadata {
        labels = {
          test = "auth"
        }
      }

      spec {
        container {
          image = "493270667162.dkr.ecr.ap-southeast-1.amazonaws.com/aug-ffm-auth:latest"
          name  = "auth"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "auth_svc" {
  metadata {
    name = "auth"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.auth.metadata.0.labels.test
    }
    
    port {
      port        = 8000
      target_port = 8000
    }

    type = "NodePort"
  }
}
